package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Программа предназначенная для скачивания картинок и музыки с сайта
 * @author Киреев Роман, Владислав Лёвин 17ИТ18
 */
public class Main extends Application {
    /**
     * Старт программы
     * @param primaryStage
     * @throws Exception
     */
    @Override

    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Музыка и Картинки");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);

    }
}
